#include <array>
#include <iostream>
#include <string>
using namespace std;

void TraditionalArrayProgram()
{
	cout << endl << string(60, '-') << endl << "TRADITIONAL ARRAY PROGRAM" << endl << endl;

	// 1. VARIABLES: Start off by declaring the following items:
    // ARR_SIZE, a const int with a value of 5.
    // itemCount, an integer with a value of 0.
    // wordArray, an array of strings of size ARR_SIZE.
	const int ARR_SIZE = 5;
	int itemCount = 0;
	string wordArray[ARR_SIZE];
	string word;


    // 2. CREATE A BASIC PROGRAM LOOP
	

	bool done = false;
	while (!done) {
		cout << "Enter a WORD to add to array or STOP to stop: ";
		cin >> word;
		if (word == "STOP" ) {
			done = true;
		}
		else if (itemCount < ARR_SIZE) {
			wordArray[itemCount] = word;
			itemCount++;
		}
		else {
			cout << endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl << "!! ERROR! Array is full !!" << endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
		}
		cout << "****************************************" << endl << "CURRENT ARRAY:" << endl;
		for (int i = 0; i < ARR_SIZE; i++) {
			cout << "wordArray[" << i << "] = " << wordArray[i] << endl;
		}
	}


		// 3. DISPLAY ALL ELEMENTS OF THE ARRAY


		// 4. ASK USER TO ENTER A NEW ITEM TO PUT IN ARRAY, OR STOP TO END.


        // 5. CHECK IF THE ARRAY IS FULL. IF NOT FULL, ADD NEW ITEM TO ARRAY.

}

void ArrayObjectProgram()
{
	cout << endl << string(60, '-') << endl << "ARRAY OBJECT PROGRAM" << endl << endl;
	
	int itemCount = 0;
	array<string, 5> wordArray;
	string word;


	// 2. CREATE A BASIC PROGRAM LOOP


	bool done = false;
	while (!done) {
		cout << "Enter a WORD to add to array or STOP to stop: ";
		cin >> word;
		if (word == "STOP") {
			done = true;
		}
		else if (itemCount < 5) {
			wordArray[itemCount] = word;
			itemCount++;
		}
		else {
			cout << endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl << "!! ERROR! Array is full !!" << endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
		}
		cout << "****************************************" << endl << "CURRENT ARRAY:" << endl;
		for (int i = 0; i < 5; i++) {
			cout << "wordArray[" << i << "] = " << wordArray[i] << endl;
		}
	}



}
