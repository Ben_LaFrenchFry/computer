#include <list>
#include <iostream>
#include <string>

using namespace std;

void ListProgram()
{
	cout << endl << string(60, '-') << endl << "LIST PROGRAM" << endl << endl;

	list<string> wordList;
	string word;
	string location;

	bool done = false;
	while (!done) {
		
		
		cout << "enter a WORD to add to the vector or STOP to stop: ";
		cin >> word;
		if (word == "STOP") {
			done = true;

		}
		else {
			cout << "Enter a location - FRONT or BACK: ";
			cin >> location;
			if (location == "front") {
				wordList.push_front(word);
			}
			else if (location == "back") {
				wordList.push_back(word);
			}
			cout << "****************************************" << endl << "CURRENT VECTOR:" << endl;
			for (auto& word : wordList) {
				cout << word << endl;
			}
			cout << endl << "Total items in vector: " << wordList.size() << endl;
		}

	

	}
}
