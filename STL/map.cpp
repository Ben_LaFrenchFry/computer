#include <map>
#include <iostream>
#include <string>
using namespace std;

void MapProgram()
{
	cout << endl << string(60, '-') << endl << "MAP PROGRAM" << endl << endl;

	map<int, string> students;
	students[1068] = "Kaz Brekker";
	students[5732] = "Inej Ghafa";
	students[1538] = "Jesper Fahey";
	students[8857] = "Nina Zenik";
	int id;

	cout << "ALL ITEMS IN MAP" << endl;
	for (auto& student : students) {
		cout << "KEY: " << student.first << " VALUE: " << student.second << endl;
	}

	cout << "\nEnter a student ID: ";
	cin >> id;
	try {
		cout << "that student is " << students.at(id);
	}
	catch (exception) {
		cout << "ERROR: there is no student with that ID!" << endl;
	}
}
