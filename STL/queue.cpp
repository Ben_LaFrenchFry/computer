#include <queue>
#include <iostream>
#include <string>
using namespace std;

void QueueProgram()
{
	cout << endl << string(60, '-') << endl << "QUEUE PROGRAM" << endl << endl;

	
	queue<string> wordQueue;
	string command = "";
	string input;
	bool isDone = false;

	cout << "COMMANDS:" << endl << "PUSH word -- add \"word\" to the top of the queue" << endl << "POP -- remove the word at the front" << endl << "STOP -- end the program and view the queue" << endl;

	while (!isDone) {
		cout << "queue size: " << wordQueue.size() << endl;
		if (wordQueue.size() > 0) {
			cout << "item at front of queue: " << wordQueue.front() << endl;
		}
		cin >> command;
		if (command == "STOP") {
			isDone = true;
			cout << endl << "FINAL QUEUE FORM:" << endl;
			int counter = 1;
			while (wordQueue.size() > 0) {
				cout << counter << ". \t" << wordQueue.front() << endl;
				wordQueue.pop();
				counter++;
			}
		}
		else {

			if (command == "PUSH") {
				cout << "Enter a word: ";
				cin >> input;
				wordQueue.push(input);
			}
			else if (command == "POP") {
				if (wordQueue.size() > 0) {
					wordQueue.pop();
				}
			}
		}

	}

}
