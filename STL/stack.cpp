#include <stack>
#include <iostream>
#include <string>
using namespace std;

void StackProgram()
{
	cout << endl << string(60, '-') << endl << "STACK PROGRAM" << endl << endl;

	// DECLARE STACK OF STRINGS
	stack<string> wordStack;
	string command = "";
	string input;
	bool isDone = false;

	cout << "COMMANDS:" << endl << "PUSH word -- add \"word\" to the top of the stack" << endl << "POP -- remove the word at the top" << endl << "STOP -- end the program and view the stack" << endl;

	while (!isDone) {
		cout << "stack size: " << wordStack.size() << endl;
		if (wordStack.size() > 0) {
			cout << "item at top of stack: " << wordStack.top() << endl;
		}
		cin >> command;
		if (command == "STOP") {
			isDone = true;
			cout << endl << "FINAL STACK FORM:" << endl;
			int counter = 1;
			while (wordStack.size() > 0) {
				cout << counter << ". \t" << wordStack.top() << endl;
				wordStack.pop();
				counter++;
			}
		}
		else {
			
			if (command == "PUSH") {
				cout << "Enter a word: ";
				cin >> input;
				wordStack.push(input);
			}
			else if (command == "POP") {
				if (wordStack.size() > 0) {
					wordStack.pop();
				}
			}
		}

	}


}
